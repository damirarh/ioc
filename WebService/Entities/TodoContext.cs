﻿using System.Data.Entity;

namespace WebService.Entities
{
    public class TodoContext : DbContext
    {
        public DbSet<TodoItem> TodoItems { get; set; } 
    }
}