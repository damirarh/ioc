﻿namespace WebService.Entities
{
    public enum Priority
    {
        Low,
        Normal,
        High
    }
}