﻿using MVVM;
using MVVM.Service;

namespace Tests
{
    public class MockWindowManager : IWindowManager
    {
        public TodoItem Item { get; set; }

        public void EditTodoItem(TodoItem item)
        {
            Item = item;
        }

        public void CloseWindow(object viewModel)
        {
            throw new System.NotImplementedException();
        }
    }
}