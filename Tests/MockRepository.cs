﻿using System.Collections.Generic;
using MVVM;
using MVVM.Service;

namespace Tests
{
    public class MockRepository : MemoryRepository
    {
        public List<TodoItem> Items
        {
            get { return _items; }
        }
    }
}