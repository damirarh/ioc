﻿using System;
using MVVM;
using MVVM.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class MainViewModelTests
    {
        [TestMethod]
        public void EditCreatesCopyOfItem()
        {
            var item = new TodoItem
                {
                    Id = 42,
                    Title = "Title",
                    Description = "Description",
                    DueDate = DateTime.Today,
                    Priority = Priority.Normal,
                    Done = false,
                };
            var repository = new MockRepository();
            repository.Items.Add(item);
            var windowManager = new MockWindowManager();
            var viewModel = new MainViewModel(repository, windowManager);
            viewModel.SelectedItem = item;

            viewModel.EditCommand.Execute(null);

            Assert.AreNotSame(item, windowManager.Item);
            Assert.AreEqual(item.Id, windowManager.Item.Id);
            Assert.AreEqual(item.Title, windowManager.Item.Title);
            Assert.AreEqual(item.Description, windowManager.Item.Description);
            Assert.AreEqual(item.DueDate, windowManager.Item.DueDate);
            Assert.AreEqual(item.Priority, windowManager.Item.Priority);
            Assert.AreEqual(item.Done, windowManager.Item.Done);
        }
    }
}
