#IoC Session Sample Application

This is the sample application for my IoC session at NT conference 2013.

First 5 branches represent different steps in the process of introducing IoC container to an existing project:

- **0-Start** contains the initial project before applying any changes to it.
- **1-Repository** isolates data access by introducing the repository pattern.
- **2-Interface** defines an interface for abstracting the repository.
- **3-Injection** moves the constructor call outside the view model class. 
- **4-Ninject** introduces an IoC container to replace manual calling of constructors and satisfying their dependencies.

The remaining branches modify the project to introduce different other features:

- **5-ServiceLocator** replaces IoC container with a service locator class.
- **6-MemoryRepository** refactors the project with a modal editing window and replaces the original repository with one storing data only to memory for the purpose of pointing out service lifetime managemant by IoC containers.
- **7-SingletonScope** reconfigures repository service as a singleton to fix the issue of losing data in the application.
- **8-Testing** adds a sample unit test based on dependency injection principle.

The best way to take a closer look at individual branches is to make a local clone of the repository and switch to the branch you are interested in. If you don't have a Git client installed you can download the source for each of the branches if you click on the [Download](https://bitbucket.org/damirarh/ioc/downloads) link and then switch to the *Branches* tab.