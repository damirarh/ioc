﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Ninject;

namespace MVVM
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public IKernel Kernel { get; private set; }

        public App()
        {
            Kernel = new StandardKernel();

            Kernel.Bind<IRepository>().To<MemoryRepository>().InSingletonScope();
            Kernel.Bind<IWindowManager>().To<WindowManager>().InSingletonScope();
            Kernel.Bind<MainViewModel>().ToSelf();
            Kernel.Bind<EditViewModel>().ToSelf();
        }
    }
}
