﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using MVVM.Annotations;
using MVVM.Service;

namespace MVVM
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<TodoItem> _todoItems;
        private TodoItem _selectedItem;

        private readonly IRepository _repository;
        private readonly IWindowManager _windowManager;

        public MainViewModel(IRepository repository, IWindowManager windowManager)
        {
            _repository = repository;
            _windowManager = windowManager;

            Init();

            NewCommand = new RelayCommand(async () => await OnNew());
            EditCommand = new RelayCommand(async () => await OnEdit());
        }

        private async Task OnEdit()
        {
            var item = new TodoItem
                {
                    Id = SelectedItem.Id,
                    Title = SelectedItem.Title,
                    Description = SelectedItem.Description,
                    DueDate = SelectedItem.DueDate,
                    Priority = SelectedItem.Priority,
                    Done = SelectedItem.Done,
                };
            _windowManager.EditTodoItem(item);
            await InitAsync();
        }

        private async Task OnNew()
        {
            var item = new TodoItem();
            _windowManager.EditTodoItem(item);
            await InitAsync();
        }

        public async void Init()
        {
            await InitAsync();
        }

        public async Task InitAsync()
        {
            TodoItems = new ObservableCollection<TodoItem>(await _repository.GetTodoItemsAsync());
        }

        public ObservableCollection<TodoItem> TodoItems
        {
            get { return _todoItems; }
            set
            {
                if (Equals(value, _todoItems)) return;
                _todoItems = value;
                OnPropertyChanged();
            }
        }

        public TodoItem SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (Equals(value, _selectedItem)) return;
                _selectedItem = value;
                OnPropertyChanged();
            }
        }

        public RelayCommand NewCommand { get; set; }
        public RelayCommand EditCommand { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}