﻿using System.Windows;
using Ninject;

namespace MVVM
{
    /// <summary>
    /// Interaction logic for EditWindow.xaml
    /// </summary>
    public partial class EditWindow : Window
    {
        public EditWindow()
        {
            InitializeComponent();
            DataContext = ((App)Application.Current).Kernel.Get<EditViewModel>();
        }
    }
}
