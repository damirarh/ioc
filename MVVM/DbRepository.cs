﻿using System.Threading.Tasks;
using MVVM.Service;

namespace MVVM
{
    public class DbRepository : IRepository
    {
        public async Task<TodoItem[]> GetTodoItemsAsync()
        {
            using (var proxy = new TodoServiceClient())
            {
                return await proxy.GetTodoItemsAsync();
            }
        }

        public async Task<TodoItem> SaveTodoItemAsync(TodoItem item)
        {
            using (var proxy = new TodoServiceClient())
            {
                return await proxy.SaveTodoItemAsync(item);
            }
        }
    }
}