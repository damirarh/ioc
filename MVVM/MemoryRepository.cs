﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MVVM.Service;

namespace MVVM
{
    public class MemoryRepository : IRepository
    {
        protected readonly List<TodoItem> _items = new List<TodoItem>();

        public async Task<TodoItem[]> GetTodoItemsAsync()
        {
            return await Task.Run(() => _items.ToArray());
        }

        public async Task<TodoItem> SaveTodoItemAsync(TodoItem item)
        {
            return await Task.Run(() =>
                {
                    if (item.Id > 0)
                    {
                        var oldItem = _items.FirstOrDefault(i => i.Id == item.Id);
                        _items.Remove(oldItem);
                    }
                    else
                    {
                        var maxId = 0;
                        if (_items.Any())
                        {
                            maxId = _items.Max(i => i.Id);
                        }
                        item.Id = maxId + 1;
                    }
                    _items.Add(item);
                    return item;
                });
        }
    }
}