﻿using System.Threading.Tasks;
using MVVM.Service;

namespace MVVM
{
    public interface IRepository
    {
        Task<TodoItem[]> GetTodoItemsAsync();
        Task<TodoItem> SaveTodoItemAsync(TodoItem item);
    }
}