﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using MVVM.Annotations;
using MVVM.Service;

namespace MVVM
{
    public class EditViewModel : INotifyPropertyChanged
    {
        private readonly IRepository _repository;
        private readonly IWindowManager _windowManager;
        private TodoItem _item;

        public EditViewModel(IRepository repository, IWindowManager windowManager)
        {
            _repository = repository;
            _windowManager = windowManager;

            CancelCommand = new RelayCommand(OnCancel);
            SaveCommand = new RelayCommand(OnSave);
        }

        private async void OnSave()
        {
            await _repository.SaveTodoItemAsync(Item);
            _windowManager.CloseWindow(this);
        }

        private void OnCancel()
        {
            _windowManager.CloseWindow(this);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public TodoItem Item
        {
            get { return _item; }
            set
            {
                if (Equals(value, _item)) return;
                _item = value;
                OnPropertyChanged();
            }
        }

        public Dictionary<Priority, string> Priorities
        {
            get
            {
                return
                    Enum.GetValues(typeof(Priority))
                        .Cast<Priority>()
                        .ToDictionary(e => e, e => Enum.GetName(typeof(Priority), e));
            }
        }

        public RelayCommand CancelCommand { get; set; }
        public RelayCommand SaveCommand { get; set; }
    }
}