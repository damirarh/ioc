﻿using MVVM.Service;

namespace MVVM
{
    public interface IWindowManager
    {
        void EditTodoItem(TodoItem item);
        void CloseWindow(object viewModel);
    }
}