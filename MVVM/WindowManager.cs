﻿using System.Collections.Generic;
using System.Windows;
using MVVM.Service;

namespace MVVM
{
    class WindowManager : IWindowManager
    {
        private readonly Dictionary<object, Window> _windows = new Dictionary<object, Window>();

        public void EditTodoItem(TodoItem item)
        {
            var editWindow = new EditWindow();
            var viewModel = (EditViewModel)editWindow.DataContext;
            viewModel.Item = item;
            _windows[viewModel] = editWindow;
            editWindow.ShowDialog();
        }

        public void CloseWindow(object viewModel)
        {
            _windows[viewModel].Close();
        }
    }
}